<?php

Route::get('/', 'Site\LoginController@index')->name('_login');
Route::get('/logoff', 'Site\LoginController@logoff')->name('_logoff');
Route::post('/authentication', 'Site\LoginController@authentication')->name('_login_autenticacao');