<?php

namespace App\Http\Controllers\Site;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
//use Ixudra\Curl\Facades\Curl;

class LoginController extends Controller
{
    public function index() {
        return view('site.login.index');
    }

    public function logoff() {
        session()->flush();
        return redirect()->route('_login');
    }

    public function authentication(Request $request) {
        $validator = Validator::make($request->only(['email', 'password']), [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);

        if (!$validator->fails()) {

            $client = new Client();
            $response = $client->request('POST', 'http://localhost/goutter2/public/auth/login', [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    'email' => $request->email,
                    'password' => $request->password
                ],
                'allow_redirects' => false,
                'timeout' => 10,
                'http_errors' => false
            ]);
            
            $json = json_decode($response->getBody());   

            if ($json->status == 'success') {
                return $json->token;
            } else {
                return $json->error;
            }

        } else {
            return redirect()->route('_logoff');
        }
    } 
}
